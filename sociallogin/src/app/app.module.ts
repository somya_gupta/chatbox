import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { Routes,RouterModule } from '@angular/router';
import{SocialLoginModule,GoogleLoginProvider, FacebookLoginProvider, AuthServiceConfig} from 'angular-6-social-login';
import{ HttpClientModule} from '@angular/common/http'

import {DataService} from './data.service';
import{AuthService} from'./auth.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './logindetails/logindetails.component';
import { ChatComponent } from './chatdetails/chatdetails.component';



import { AuthchatService } from './authchat.service';
import { NotfoundComponent } from './notfound/notfound.component';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("360715922960-9b8ukqad8nn491ooe1td513jh6hfposc.apps.googleusercontent.com")
      },
      {
        id:FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("1752990788155492")
      }

    ]
);
  return config;
}

 const routes:Routes=[
   {
     path:'',
     component:LoginComponent,
     canActivate:[AuthchatService]
   },
   {
     path:'chat',
     component:ChatComponent,
     canActivate:[AuthService]
   },
   {
     path:'**',
     component:NotfoundComponent
   }
   
 ]
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChatComponent,
    NotfoundComponent,
 
  
    
  ],
  imports: [
    FormsModule,
    BrowserModule,
    SocialLoginModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    DataService,
    AuthService,
    AuthchatService,
    
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
