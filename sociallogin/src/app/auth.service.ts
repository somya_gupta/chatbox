import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  constructor(private router:Router) { }
  canActivate(){
    if(sessionStorage.getItem("key1")=="somya=="){
      return true;
    }
    alert('Time Up');
    this.router.navigate(['/']);
    return false;
  }
}
