import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthchatService  implements CanActivate {

  constructor(private router:Router) { }
  canActivate(){
    if(sessionStorage.getItem("key2")=="gupta=="){
      alert('You are logged in.');
      this.router.navigate(['/chat']);
      return false;
    }
    return true;
  }
}