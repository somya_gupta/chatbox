import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic QUMwOThkMGUyN2RhZDg2MjViZDRhOWM3M2JlZjE4YmVkOTo4ZTdlNWU5ZWE5NTVhZjU3NTAyY2E2Y2UxZmIwZjJjZA=='
    })
  };
  constructor(private http: HttpClient) { }

  createservice(identity,friendlyname): Observable<any> {

    return this.http.post<any>('https://chat.twilio.com/v2/Services/'+serviceId+"/Users",
     "FriendlyName="+friendlyname+"&Identity="+identity+"&ServiceSid="+serviceId, this.httpOptions);
   
  }

  GeneralChannel(identity): Observable<any> {

    return this.http.post<any>('https://chat.twilio.com/v2/Services/'+serviceId+"/Channels/general/Members",
     "UniqueName =general&Identity="+identity+"&ServiceSid="+serviceId, this.httpOptions);

  }

  AllChannel(): Observable<any> {

    return this.http.get('https://chat.twilio.com/v2/Services/'+serviceId+"/Channels/",
     this.httpOptions);

  }

  addUserChannel(unique_name,identity): Observable<any> {
    return this.http.post<any>('https://chat.twilio.com/v2/Services/'+serviceId+"/Channels/"+unique_name+"/Members",
     "UniqueName ="+unique_name+"&Identity="+identity+"&ServiceSid="+serviceId, this.httpOptions);

  }

  getUserChannel(identity): Observable<any> {

    return this.http.get('https://chat.twilio.com/v2/Services/'+serviceId+"/Users/"+identity+"/Channels/",
     this.httpOptions);

  }

  createChannel(channelName): Observable<any> {
    console.log(channelName)
        return this.http.post<any>('https://chat.twilio.com/v2/Services/'+serviceId+"/Channels/",
         "UniqueName="+channelName+"&ServiceSid="+serviceId, this.httpOptions);
      }

  getMessage(): Observable<any> {
    var channel = sessionStorage.getItem("channel");
    return this.http.get('https://chat.twilio.com/v2/Services/'+serviceId+"/Channels/"+channel+"/Messages",
    this.httpOptions);
  }

  sendMessage(msg,from): Observable<any> {
    var channel= sessionStorage.getItem("channel");
        return this.http.post<any>('https://chat.twilio.com/v2/Services/'+serviceId+"/Channels/"+channel+"/Messages",
         "UniqueName="+channel+"&Body="+msg+"&From="+from+"&ServiceSid="+serviceId, this.httpOptions);
    
      }

}
const serviceId:string='IS26bc261dc86f401b8fd38fe87a59df6d';
